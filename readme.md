# ni

## What
A network editor for [xir](https://gitlab.com/mergetb/xir) networks.

## Why
Expressing large complex network topologies is hard. Doing so in a programming environment where one can use data structures and functional abstractions to manage complexity helps tremendously. Experience with large and complex networks has shown that good visualizations are also extremely useful in assessing whether or not the network that one has expressed is indeed the intended network. There are many algorithms out there for network visualization. However, in practice they rarely get us to the visual representation we really hope for on their own. Furthermore, which visualization technique or set of algorithms to use in many cases depends on the network being rendered. Finally, when a network gets big enough we seldom want to see the whole thing at a full level of detail in one view. We would rather view specific aspects in detail while abstracting away others that are not as relevant to what the visualization is designed to elucidate.

What we want is a network editor that allows a designer to interact with a visualization through higher order functions. We propose 4 function types as a basis.

- Modifiers:   `(Network, Shapes, View, Selection) -> Network)`
- Abstractors: `(Network, Shapes, View, Selection) -> Shapes)`
- Apertures:   `(Network, Shapes, View, Selection) -> View)`
- Selectors:   `(Network, Shapes, View, Selection) -> Selection)`

Each function takes the same input, the visual state space. This state space is composed of

- Network: The network being visualized
- Shapes: A map that assigns to each element in the network a shape abstraction that may be rendered graphically. This map is surjective in nature, so many distinct network objects may map to a single shape abstraction.
- View: The state associated with viewport in which the network is being rendered e.g., height, width, perspective distance, clipping planes etc.
- Selection: What elements in a visualization are currently selected. This is a surjective map of shape elements onto boolean values.

The higher-order functions associated with each of these types carry the following semantics

- Modifiers: Modify the network definition itself, including adding and removing elements.
- Abstractors: Define how subsets of network elements map into shape abstractions.
- Apertures: Define the properties of the visualization viewport.
- Selectors: Define what elements in a visualization are selected.

A designer interacts with a visualization by _defining_ and _applying_ a basis function. When a basis function is applied a new copy of the visualization state space is made and the basis function is applied to that copy. The updated state as well as the basis function are each pushed onto a stack so a history of states and associated function applications are maintained, allowing for flexible undo, roll-back, roll-forward and cherry picking mechanisms.

## Dependencies

- [mypy](http://mypy-lang.org/)
- [svgwrite](https://pypi.org/project/svgwrite/)
- [grpcio-tools](https://pypi.org/project/grpcio-tools/)
- [googleapis-common-protos](https://pypi.org/project/googleapis-common-protos/)
- [xir](https://gitlab.com/mergetb/xir/tree/master/lang/python)
- [grpc-web](https://gitlab.com/mergetb/fedorka/-/jobs/94345056/artifacts/browse/build/)
