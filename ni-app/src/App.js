import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

//import * as ni from './proto/ni_grpc_web_pb.js';
var ni = require('./proto/ni_grpc_web_pb.js');

function sayHello() {
  var request = new ni.Empty();
  var niService = new ni.NiClient('http://localhost:6000');
  var metadata = {};
  var call = niService.health(request, metadata, function(err, response) {
    if(err) {
      console.log(err.code);
      console.log(err.message);
    } else {
      alert("health is " + response.getMessage());
    }
  })
  call.on('status', function(status) {
    console.log(status.code);
    console.log(status.details);
    console.log(status.metadata);
  });
}

class App extends Component {
  render() {
    sayHello();
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

export default App;
