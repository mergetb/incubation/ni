/* eslint-disable */
/**
 * @fileoverview gRPC-Web generated client stub for ni
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.ni = require('./ni_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.ni.NiClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.ni.NiPromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!proto.ni.NiClient} The delegate callback based client
   */
  this.delegateClient_ = new proto.ni.NiClient(
      hostname, credentials, options);

};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ni.PyFunc,
 *   !proto.ni.VDiff>}
 */
const methodInfo_Modify = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ni.VDiff,
  /** @param {!proto.ni.PyFunc} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.ni.VDiff.deserializeBinary
);


/**
 * @param {!proto.ni.PyFunc} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ni.VDiff)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ni.VDiff>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ni.NiClient.prototype.modify =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ni.Ni/Modify',
      request,
      metadata,
      methodInfo_Modify,
      callback);
};


/**
 * @param {!proto.ni.PyFunc} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ni.VDiff>}
 *     The XHR Node Readable Stream
 */
proto.ni.NiPromiseClient.prototype.modify =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.modify(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ni.PyFunc,
 *   !proto.ni.VDiff>}
 */
const methodInfo_Abstract = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ni.VDiff,
  /** @param {!proto.ni.PyFunc} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.ni.VDiff.deserializeBinary
);


/**
 * @param {!proto.ni.PyFunc} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ni.VDiff)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ni.VDiff>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ni.NiClient.prototype.abstract =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ni.Ni/Abstract',
      request,
      metadata,
      methodInfo_Abstract,
      callback);
};


/**
 * @param {!proto.ni.PyFunc} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ni.VDiff>}
 *     The XHR Node Readable Stream
 */
proto.ni.NiPromiseClient.prototype.abstract =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.abstract(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ni.PyFunc,
 *   !proto.ni.VDiff>}
 */
const methodInfo_View = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ni.VDiff,
  /** @param {!proto.ni.PyFunc} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.ni.VDiff.deserializeBinary
);


/**
 * @param {!proto.ni.PyFunc} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ni.VDiff)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ni.VDiff>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ni.NiClient.prototype.view =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ni.Ni/View',
      request,
      metadata,
      methodInfo_View,
      callback);
};


/**
 * @param {!proto.ni.PyFunc} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ni.VDiff>}
 *     The XHR Node Readable Stream
 */
proto.ni.NiPromiseClient.prototype.view =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.view(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ni.PyFunc,
 *   !proto.ni.VDiff>}
 */
const methodInfo_Select = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ni.VDiff,
  /** @param {!proto.ni.PyFunc} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.ni.VDiff.deserializeBinary
);


/**
 * @param {!proto.ni.PyFunc} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ni.VDiff)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ni.VDiff>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ni.NiClient.prototype.select =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ni.Ni/Select',
      request,
      metadata,
      methodInfo_Select,
      callback);
};


/**
 * @param {!proto.ni.PyFunc} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ni.VDiff>}
 *     The XHR Node Readable Stream
 */
proto.ni.NiPromiseClient.prototype.select =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.select(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.ni.Empty,
 *   !proto.ni.HealthStatus>}
 */
const methodInfo_Health = new grpc.web.AbstractClientBase.MethodInfo(
  proto.ni.HealthStatus,
  /** @param {!proto.ni.Empty} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.ni.HealthStatus.deserializeBinary
);


/**
 * @param {!proto.ni.Empty} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.ni.HealthStatus)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.ni.HealthStatus>|undefined}
 *     The XHR Node Readable Stream
 */
proto.ni.NiClient.prototype.health =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/ni.Ni/Health',
      request,
      metadata,
      methodInfo_Health,
      callback);
};


/**
 * @param {!proto.ni.Empty} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.ni.HealthStatus>}
 *     The XHR Node Readable Stream
 */
proto.ni.NiPromiseClient.prototype.health =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.health(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


module.exports = proto.ni;

